const mongoose = require('mongoose');


const dbConnection = async () => {

    try {
        // abel_db - - - ZMzLkuRFLgv9oO81
        await mongoose.connect(process.env.BD_CNN, {
            
        });
        
        console.log('DB Online!');

    } catch (error) {
        console.log(error);
        throw new Error('Error al iniciar la database');
        
    }
    
}

module.exports = {
    dbConnection
}