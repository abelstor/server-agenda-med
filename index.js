require('dotenv').config();

const express = require('express');
const cors = require('cors');

const { dbConnection } = require('./database/config');

// Crear servidor de express
const app = express();

// Configurar CORS
app.use(cors());

// Lectura y parseo del body
app.use(express.json());

// Base de datos
dbConnection();

// Directorio público
app.use( express.static('public'));

// Rutas
app.use( '/api/distritos', require('./routes/distritos'));
app.use( '/api/usuarios', require('./routes/usuarios'));
app.use( '/api/agentes', require('./routes/agentes'));
app.use( '/api/upload', require('./routes/uploads'));
app.use( '/api/login', require('./routes/auth'));
app.use( '/api/todo', require('./routes/busquedas'));


app.listen(process.env.PORT, () => {
    console.log('Servidor corriendo en el puerto ' + process.env.PORT);
});