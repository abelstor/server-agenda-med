/*
    Agentes
    Path: '/api/agentes'
*/
const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');

const { validarJWT } = require('../middlewares/validar-jwt');

const { 
    getAgentes, 
    crearAgente,
    actualizarAgente,
    borrarAgente
} = require('../controllers/agentes');

const router = Router();

router.get( '/', getAgentes );

router.post( '/',
    [
        validarJWT,
        check('nombre', 'El nombre del Agente es obligatorio').not().isEmpty(),
        check('distrito', 'El Distrito ID debe ser válido').isMongoId(),
        validarCampos
    ], 
    crearAgente
);

router.put( '/:id',
    [
        validarJWT,
        check('nombre', 'El nombre del Agente es obligatorio').not().isEmpty(),
        check('distrito', 'El Distrito ID debe ser válido').isMongoId(),
        validarCampos
    ], 
    actualizarAgente 
);

router.delete( '/:id',
    borrarAgente
);


module.exports = router;

