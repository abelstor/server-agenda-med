/*
    Distritos
    Path: '/api/distritos'
*/
const { Router } = require('express');
const { check } = require('express-validator');
const { validarCampos } = require('../middlewares/validar-campos');

const { validarJWT } = require('../middlewares/validar-jwt');

const { 
    getDistritos, 
    crearDistrito,
    actualizarDistrito,
    borrarDistrito
} = require('../controllers/distritos')

const router = Router();

router.get( '/', getDistritos );

router.post( '/',
    [
        validarJWT,
        check('nombre', 'El nombre del Distrito es necesario').not().isEmpty(),
        validarCampos
    ], 
    crearDistrito
);

router.put( '/:id',
    [
        validarJWT,
        check('nombre', 'El nombre del Distrito es necesario').not().isEmpty(),
        validarCampos
    ], 
    actualizarDistrito 
);

router.delete( '/:id',
    validarJWT,
    borrarDistrito
);


module.exports = router;

