const { Schema, model } = require('mongoose');

const AgenteSchema = Schema({

    nombre: {
        type: String,
        required: true,
    },
    img: {
        type: String,
    },
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        required: true
    },
    distrito: {
        type: Schema.Types.ObjectId,
        ref: 'Distrito',
        required: true
    }
});

AgenteSchema.method('toJSON', function() {
    const { __v, ...object } = this.toObject();
    return object;
});


module.exports = model( 'Agente', AgenteSchema );