const fs = require('fs');

const Usuario = require('../models/usuario');
const Agente = require('../models/agente');
const Distrito = require('../models/distrito');

const borrarImg = ( path ) => {
    if ( fs.existsSync( path )) {
        // borrar la imagen anterior
        fs.unlinkSync( path );
    }
}

const actualizarImg = async ( tipo, id, nombreArchivo ) => {

    let pathViejo = '';

    switch (tipo) {
        case 'agentes':
            const agente = await Agente.findById(id);
            if( !agente ) {
                console.log('No es un Agente por id');
                return false;
            }

            pathViejo = `./uploads/agentes/${ agente.img }`;
            borrarImg( pathViejo );

            agente.img = nombreArchivo;
            await agente.save();
            return true;

            break;

        case 'distritos':
            const distrito = await Distrito.findById(id);
            if( !distrito ) {
                console.log('No es un Distrito por id');
                return false;
            }

            pathViejo = `./uploads/distritos/${ distrito.img }`;
            borrarImg( pathViejo );

            distrito.img = nombreArchivo;
            await distrito.save();
            return true;
            
            break;

        case 'usuarios':
            const usuario = await Usuario.findById(id);
            if( !usuario ) {
                console.log('No es un Usuario por id');
                return false;
            }

            pathViejo = `./uploads/usuarios/${ usuario.img }`;
            borrarImg( pathViejo );

            usuario.img = nombreArchivo;
            await usuario.save();
            return true;
            
            break;
    
        default:
            return res.status(400).json({
                ok: false,
                msg: 'Busqueda solo por: usuarios/agentes/distritos'
            });
    }




}


module.exports = {
    actualizarImg
};