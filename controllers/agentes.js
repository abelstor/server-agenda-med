const { response } = require('express');

const Agente = require('../models/agente');

const getAgentes = async (req, res = response) => {

    const agentes = await Agente.find()
                                .populate('usuario', 'nombre img')
                                .populate('distrito', 'nombre img')

    res.json({
        ok: true,
        agentes
    })
}

const crearAgente = async(req, res = response) => {

    const uid = req.uid;
    const agente = new Agente({
        usuario: uid,
        ...req.body
    });

    try {

        const agenteDB = await agente.save();
        
        res.json({
            ok: true,
            agente: agenteDB
        })

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
        
    }

}

const actualizarAgente = async(req, res = response) => {

    const id = req.params.id;
    const uid = req.uid;

    try {

        const agente = await Agente.findById( id );

        if (!agente ) {
            return res.status(404).json({
                ok: true,
                msg: 'Agente no encontrado por id',
            });
        }

        const cambiosAgente = {
            ...req.body,
            usuario: uid
        };

        const agenteActualizado = await Agente.findByIdAndUpdate( id, cambiosAgente, { new: true } );


        res.json({
            ok: true,
            agente: agenteActualizado
        });
        
    } catch (error) {

        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}

const borrarAgente = async(req, res = response) => {

    const id = req.params.id;

    try {

        const agente = await Agente.findById( id );

        if (!agente ) {
            return res.status(404).json({
                ok: true,
                msg: 'Agente no encontrado por id',
            });
        }

        await Agente.findByIdAndDelete( id );

        res.json({
            ok: true,
            msg: 'Agente Eliminado'
        });
        
    } catch (error) {

        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}



module.exports = {
    getAgentes,
    crearAgente,
    actualizarAgente,
    borrarAgente
}