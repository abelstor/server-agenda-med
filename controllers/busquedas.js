const { response } = require('express');

const Usuario = require('../models/usuario');
const Distrito = require('../models/distrito');
const Agente = require('../models/agente');

const getTodo = async ( req, res = response ) => {

    const busqueda = req.params.busqueda;

    const regex = new RegExp( busqueda, 'i');   
    
    
    const [ usuarios, agentes, distritos ] = await Promise.all([

        Usuario.find({ nombre: regex }),
        Distrito.find({ nombre: regex }),
        Agente.find({ nombre: regex }),
    ]);


    res.json({
        ok: true,
        usuarios,
        distritos,
        agentes
    });

}

const getDocumentosColeccion = async ( req, res = response ) => {

    const tabla    = req.params.tabla;
    const busqueda = req.params.busqueda;
    const regex = new RegExp( busqueda, 'i');

    let data = [];
    
    switch (tabla) {
        case 'agentes':
            data = await Agente.find({ nombre: regex })
                               .populate('usuario', 'nombre img')
                               .populate('distrito', 'nombre img');
            break;

        case 'distritos':
            data = await Distrito.find({ nombre: regex })
                                 .populate('usuario', 'nombre img');
            break;

        case 'usuarios':
            data = await Usuario.find({ nombre: regex });
            
            break;
    
        default:
            return res.status(400).json({
                ok: false,
                msg: 'Busqueda solo por: usuarios/agentes/distritos'
            });
    }

    res.json({
        ok: true,
        resultados: data
    });

}


module.exports = {
    getTodo,
    getDocumentosColeccion
}