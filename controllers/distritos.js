const { response } = require('express');

const Distrito = require('../models/distrito');

const getDistritos = async (req, res = response) => {

    const distritos = await Distrito.find()
                                    .populate('usuario', 'nombre img')

    res.json({
        ok: true,
        distritos
    });
}

const crearDistrito = async(req, res = response) => {

    const uid = req.uid;
    const distrito = new Distrito({
        usuario: uid,
        ...req.body
    });

    try {
        
        const distritoDB = await distrito.save();

        res.json({
            ok: true,
            distrito: distritoDB
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        })
    }

}

const actualizarDistrito = async(req, res = response) => {

    const id = req.params.id;
    const uid = req.uid;

    try {

        const distrito = await Distrito.findById( id );

        if (!distrito ) {
            return res.status(404).json({
                ok: true,
                msg: 'Distrito no encontrado por id',
            });
        }

        //distrito.nombre = req.body.nombre;
        const cambiosDistrito = {
            ...req.body,
            usuario: uid
        };

        const distritoActualizado = await Distrito.findByIdAndUpdate( id, cambiosDistrito, { new: true } );


        res.json({
            ok: true,
            distrito: distritoActualizado
        });
        
    } catch (error) {

        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    } 
}

const borrarDistrito = async(req, res = response) => {


    const id = req.params.id;

    try {

        const distrito = await Distrito.findById( id );

        if (!distrito ) {
            return res.status(404).json({
                ok: true,
                msg: 'Distrito no encontrado por id',
            });
        }

        await Distrito.findByIdAndDelete( id );

        res.json({
            ok: true,
            msg: 'Distrito eliminado'
        });
        
    } catch (error) {

        console.log(error);
        res.status(500).json({
            ok: false,
            msg: 'Hable con el administrador'
        });
    }
}



module.exports = {
    getDistritos,
    crearDistrito,
    actualizarDistrito,
    borrarDistrito
}